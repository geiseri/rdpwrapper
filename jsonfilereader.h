/*
 * Copyright (C) 2017  TCOS Maintainers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef JSONFILEREADER_H
#define JSONFILEREADER_H

#include "abstractfilereader.h"
#include <QJsonDocument>

/*
{
    "server" : "",
    "username" : "",
    "domain" : "",
    "password" : "",
    "branding_url" : "http://server/path/to/icon.png",
    "bpp" : 16,
    "reconnect_max_retries" : 0,
    "remote_fx" : false,
    "cleartype_fonts" : false,
    "aero_effects" : false,
    "window_drag" : false,
    "menu_animations" : false,
    "themes" : false,
    "wallpaper" : false,
    "heartbeat" : false,
    "multitransport" : false,
    "auto_reconnect" : false,
    "display_control" : false,
    "echo" : false,
    "multimon" : false,
    "sound_output" : false,
    "sound_input" : false,
    "ignore_ssl_errors" : false
}
*/

class JsonFileReader : public AbstractFileReader
{
public:
    JsonFileReader();

    virtual bool configure(const QString &file) Q_DECL_OVERRIDE;
    virtual QString server(const QString &defaultValue) const Q_DECL_OVERRIDE;
    virtual QString username(const QString &defaultValue) const Q_DECL_OVERRIDE;
    virtual QString domain(const QString &defaultValue) const Q_DECL_OVERRIDE;
    virtual QString password(const QString &defaultValue) const Q_DECL_OVERRIDE;
    virtual QString brandingUrl(const QString &defaultValue) const Q_DECL_OVERRIDE;
    virtual QString termsOfService(const QString &defaultValue) const Q_DECL_OVERRIDE;
    virtual int bpp(int defaultValue) const Q_DECL_OVERRIDE;
    virtual int retries(int defaultValue) const Q_DECL_OVERRIDE;
    virtual bool remoteFx(bool defaultValue) const Q_DECL_OVERRIDE;
    virtual bool cleartype(bool defaultValue) const Q_DECL_OVERRIDE;
    virtual bool aero(bool defaultValue) const Q_DECL_OVERRIDE;
    virtual bool windowDrag(bool defaultValue) const Q_DECL_OVERRIDE;
    virtual bool animations(bool defaultValue) const Q_DECL_OVERRIDE;
    virtual bool themes(bool defaultValue) const Q_DECL_OVERRIDE;
    virtual bool wallpaper(bool defaultValue) const Q_DECL_OVERRIDE;
    virtual bool heartbeat(bool defaultValue) const Q_DECL_OVERRIDE;
    virtual bool multitransport(bool defaultValue) const Q_DECL_OVERRIDE;
    virtual bool reconnect(bool defaultValue) const Q_DECL_OVERRIDE;
    virtual bool displayControl(bool defaultValue) const Q_DECL_OVERRIDE;
    virtual bool echoChannel(bool defaultValue) const Q_DECL_OVERRIDE;
    virtual bool multiMon(bool defaultValue) const Q_DECL_OVERRIDE;
    virtual bool sound(bool defaultValue) const Q_DECL_OVERRIDE;
    virtual bool microphone(bool defaultValue) const Q_DECL_OVERRIDE;
    virtual bool ignoreCertificates(bool defaultValue) const Q_DECL_OVERRIDE;
    virtual bool redirectDrives(bool defaultValue) const Q_DECL_OVERRIDE;
    virtual bool useCredSSP(bool defaultValue) const Q_DECL_OVERRIDE;
    virtual bool useCompression(bool defaultValue) const Q_DECL_OVERRIDE;
    virtual bool clipboard(bool defaultValue) const Q_DECL_OVERRIDE;

private:
    QJsonDocument m_document;

};

#endif // JSONFILEREADER_H
