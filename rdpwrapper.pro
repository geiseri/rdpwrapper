QT       += core gui widgets network

TARGET = rdpwrapper
TEMPLATE = app

SOURCES += \
        main.cpp \
        logindialog.cpp \
        rdprunner.cpp \
        rdpfilereader.cpp \
        abstractfilereader.cpp \
        jsonfilereader.cpp

HEADERS += \
        logindialog.h \
        rdprunner.h \
        rdpfilereader.h \
        abstractfilereader.h \
        jsonfilereader.h

FORMS += \
        logindialog.ui

DISTFILES += \
    test.json \
    COPYING

target.path = /usr/bin
INSTALLS += target

CONFIG += link_pkgconfig
PKGCONFIG += qtsystemd
