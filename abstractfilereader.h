/*
 * Copyright (C) 2017  TCOS Maintainers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ABSTRACTFILEREADER_H
#define ABSTRACTFILEREADER_H

#include <QJsonDocument>
#include <QJsonObject>
#include <QVariant>

class AbstractFileReader
{
public:
    AbstractFileReader();
    virtual ~AbstractFileReader();
    virtual bool configure(const QString &file) = 0;

    virtual QString server(const QString &defaultValue = QString()) const = 0;
    virtual QString username(const QString &defaultValue = QString()) const = 0;
    virtual QString domain(const QString &defaultValue = QString()) const = 0;
    virtual QString password(const QString &defaultValue = QString()) const = 0;
    virtual QString brandingUrl(const QString &defaultValue = QString()) const = 0;
    virtual QString termsOfService(const QString &defaultValue = QString()) const = 0;
    virtual int bpp(int defaultValue = 16) const = 0;
    virtual int retries(int defaultValue = 0) const = 0;
    virtual bool remoteFx(bool defaultValue = false) const = 0;
    virtual bool cleartype(bool defaultValue = false) const = 0;
    virtual bool aero(bool defaultValue = false) const = 0;
    virtual bool windowDrag(bool defaultValue = false) const = 0;
    virtual bool animations(bool defaultValue = false) const = 0;
    virtual bool themes(bool defaultValue = false) const = 0;
    virtual bool wallpaper(bool defaultValue = false) const = 0;
    virtual bool heartbeat(bool defaultValue = false) const = 0;
    virtual bool multitransport(bool defaultValue = false) const = 0;
    virtual bool reconnect(bool defaultValue = false) const = 0;
    virtual bool displayControl(bool defaultValue = false) const = 0;
    virtual bool echoChannel(bool defaultValue = false) const = 0;
    virtual bool multiMon(bool defaultValue = false) const = 0;
    virtual bool sound(bool defaultValue = false) const = 0;
    virtual bool microphone(bool defaultValue = false) const = 0;
    virtual bool ignoreCertificates(bool defaultValue = false) const = 0;
    virtual bool redirectDrives(bool defaultValue = false) const = 0;
    virtual bool useCredSSP(bool defaultValue = false) const = 0;
    virtual bool useCompression( bool defaultValue = false ) const = 0;
    virtual bool clipboard( bool defaultValue = false ) const = 0;

protected:
    template <typename Y>
    Y extractMapValue(const QVariantMap &config, const QString &name, const Y &defaultValue) const
    {
        if (config.contains(name)) {
            return config[name].value<Y>();
        } else {
            return defaultValue;
        }
    }
};

#endif // ABSTRACTFILEREADER_H
