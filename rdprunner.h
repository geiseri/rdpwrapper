/*
 * Copyright (C) 2017  TCOS Maintainers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RDPRUNNER_H
#define RDPRUNNER_H

#include <QObject>
#include <QProcess>
#include <QVariantMap>

class AbstractFileReader;
class QUrl;
class QLabel;
class QNetworkAccessManager;

class RdpRunner : public QObject
{
    Q_OBJECT
public:
    explicit RdpRunner(QObject *parent = nullptr);
    bool configure(const QString &configFile);

    QString server() const;
    QString username() const;
    QString domain() const;
    QString password() const;
    QUrl brandingUrl() const;
    QString termsOfService() const;

signals:
    void running(bool isRunning);
    void sslError();
    void authError();
    void connectionError();
    void finished( int error );

public slots:
    void connectToServer(const QString &username, const QString &domain, const QString &password);
    void populateBrandingImage(QLabel *target) const;

private slots:
    void onStdError();
    void onStdOut();
    void onExit(int exitCode);
    void onStateChange(QProcess::ProcessState state);

private:
    QStringList args(const QString &username, const QString &domain, const QString &password) const;
    QString enableFlag(const QString &flag, bool enable) const;
    QString rdpArg(const QString &arg, const QString &value) const;

    QProcess *m_rdp;
    AbstractFileReader *m_configuration;
    QNetworkAccessManager *m_nam;
};

#endif // RDPRUNNER_H
