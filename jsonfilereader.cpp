#include "jsonfilereader.h"

/*
 * Copyright (C) 2017  TCOS Maintainers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <QFile>
#include <QJsonObject>
#include <QJsonParseError>

JsonFileReader::JsonFileReader() : AbstractFileReader()
{
}

bool JsonFileReader::configure(const QString &file)
{
    QJsonParseError parserError;
    QFile json(file);
    if (json.open(QIODevice::ReadOnly)) {
        m_document = QJsonDocument::fromJson(json.readAll(), &parserError);
        return (parserError.error == QJsonParseError::NoError && !server(QString()).isEmpty());
    }

    return false;
}

QString JsonFileReader::server(const QString &defaultValue) const
{
    return extractMapValue<QString>(m_document.object().toVariantMap(), QLatin1String("server"), defaultValue);
}

QString JsonFileReader::username(const QString &defaultValue) const
{
    return extractMapValue<QString>(m_document.object().toVariantMap(), QLatin1String("username"), defaultValue);
}

QString JsonFileReader::domain(const QString &defaultValue) const
{
    return extractMapValue<QString>(m_document.object().toVariantMap(), QLatin1String("domain"), defaultValue);
}

QString JsonFileReader::password(const QString &defaultValue) const
{
    return extractMapValue<QString>(m_document.object().toVariantMap(), QLatin1String("password"), defaultValue);
}

QString JsonFileReader::brandingUrl(const QString &defaultValue) const
{
    return extractMapValue<QString>(m_document.object().toVariantMap(), QLatin1String("branding_url"), defaultValue);
}

QString JsonFileReader::termsOfService(const QString &defaultValue) const
{
    return extractMapValue<QString>(
        m_document.object().toVariantMap(), QLatin1String("terms_of_service"), defaultValue);
}

int JsonFileReader::bpp(int defaultValue) const
{
    return extractMapValue<int>(m_document.object().toVariantMap(), QLatin1String("bpp"), defaultValue);
}

int JsonFileReader::retries(int defaultValue) const
{
    return extractMapValue<int>(
        m_document.object().toVariantMap(), QLatin1String("reconnect_max_retries"), defaultValue);
}

bool JsonFileReader::remoteFx(bool defaultValue) const
{
    return extractMapValue<bool>(m_document.object().toVariantMap(), QLatin1String("remote_fx"), defaultValue);
}

bool JsonFileReader::cleartype(bool defaultValue) const
{
    return extractMapValue<bool>(m_document.object().toVariantMap(), QLatin1String("cleartype_fonts"), defaultValue);
}

bool JsonFileReader::aero(bool defaultValue) const
{
    return extractMapValue<bool>(m_document.object().toVariantMap(), QLatin1String("aero_effects"), defaultValue);
}

bool JsonFileReader::windowDrag(bool defaultValue) const
{
    return extractMapValue<bool>(m_document.object().toVariantMap(), QLatin1String("window_drag"), defaultValue);
}

bool JsonFileReader::animations(bool defaultValue) const
{
    return extractMapValue<bool>(m_document.object().toVariantMap(), QLatin1String("menu_animations"), defaultValue);
}

bool JsonFileReader::themes(bool defaultValue) const
{
    return extractMapValue<bool>(m_document.object().toVariantMap(), QLatin1String("themes"), defaultValue);
}

bool JsonFileReader::wallpaper(bool defaultValue) const
{
    return extractMapValue<bool>(m_document.object().toVariantMap(), QLatin1String("wallpaper"), defaultValue);
}

bool JsonFileReader::heartbeat(bool defaultValue) const
{
    return extractMapValue<bool>(m_document.object().toVariantMap(), QLatin1String("heartbeat"), defaultValue);
}

bool JsonFileReader::multitransport(bool defaultValue) const
{
    return extractMapValue<bool>(m_document.object().toVariantMap(), QLatin1String("multitransport"), defaultValue);
}

bool JsonFileReader::reconnect(bool defaultValue) const
{
    return extractMapValue<bool>(m_document.object().toVariantMap(), QLatin1String("auto_reconnect"), defaultValue);
}

bool JsonFileReader::displayControl(bool defaultValue) const
{
    return extractMapValue<bool>(m_document.object().toVariantMap(), QLatin1String("display_control"), defaultValue);
}

bool JsonFileReader::echoChannel(bool defaultValue) const
{
    return extractMapValue<bool>(m_document.object().toVariantMap(), QLatin1String("echo"), defaultValue);
}

bool JsonFileReader::multiMon(bool defaultValue) const
{
    return extractMapValue<bool>(m_document.object().toVariantMap(), QLatin1String("multimon"), defaultValue);
}

bool JsonFileReader::sound(bool defaultValue) const
{
    return extractMapValue<bool>(m_document.object().toVariantMap(), QLatin1String("sound_output"), defaultValue);
}

bool JsonFileReader::microphone(bool defaultValue) const
{
    return extractMapValue<bool>(m_document.object().toVariantMap(), QLatin1String("sound_input"), defaultValue);
}

bool JsonFileReader::ignoreCertificates(bool defaultValue) const
{
    return extractMapValue<bool>(m_document.object().toVariantMap(), QLatin1String("ignore_ssl_errors"), defaultValue);
}

bool JsonFileReader::redirectDrives(bool defaultValue) const
{
    return extractMapValue<bool>(m_document.object().toVariantMap(), QLatin1String("redirect_drives"), defaultValue);
}

bool JsonFileReader::useCredSSP(bool defaultValue) const
{
    return extractMapValue<bool>(m_document.object().toVariantMap(), QLatin1String("enable_nla"), defaultValue);
}

bool JsonFileReader::useCompression(bool defaultValue) const
{
    return extractMapValue<bool>(m_document.object().toVariantMap(), QLatin1String("enable_compression"), defaultValue);
}

bool JsonFileReader::clipboard(bool defaultValue) const
{
    return extractMapValue<bool>(m_document.object().toVariantMap(), QLatin1String("clipboard"), defaultValue);
}
