/*
 * Copyright (C) 2017  TCOS Maintainers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "rdprunner.h"
#include "jsonfilereader.h"
#include "rdpfilereader.h"
#include <QCoreApplication>
#include <QDebug>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QLabel>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QProcess>
#include <QUrl>

RdpRunner::RdpRunner(QObject *parent) : QObject(parent), m_configuration(0), m_nam(new QNetworkAccessManager(this))
{
    m_rdp = new QProcess(this);
    connect(m_rdp, &QProcess::readyReadStandardError, this, &RdpRunner::onStdError);
    connect(m_rdp, &QProcess::readyReadStandardOutput, this, &RdpRunner::onStdOut);
    connect(m_rdp, &QProcess::stateChanged, this, &RdpRunner::onStateChange);

    // broken until Qt 6
    connect(m_rdp, SIGNAL(finished(int)), this, SLOT(onExit(int)));
}

template <typename T>
T extractArg(const QJsonObject &config, const QString &name, const T &defaultValue)
{
    if (config.contains(name)) {
        return config[name].toVariant().value<T>();
    } else {
        return defaultValue;
    }
}

bool RdpRunner::configure(const QString &configFile)
{
    if (m_configuration) {
        delete m_configuration;
        m_configuration = 0;
    }

    if (configFile.endsWith(QLatin1String(".rdp"))) {
        m_configuration = new RdpFileReader();
    } else if (configFile.endsWith(QLatin1String(".json"))) {
        m_configuration = new JsonFileReader();
    } else {
        return false;
    }

    return m_configuration->configure(configFile);
}

void RdpRunner::connectToServer(const QString &username, const QString &domain, const QString &password)
{
    m_rdp->setProgram(QLatin1String("/usr/bin/xfreerdp"));
    m_rdp->setArguments(args(username, domain, password));
    m_rdp->start();
}

void RdpRunner::populateBrandingImage(QLabel *target) const
{
    QNetworkRequest fetchRequest = QNetworkRequest(brandingUrl());

    fetchRequest.setHeader(QNetworkRequest::UserAgentHeader, QLatin1String("rdpwrapper/1.0"));
    fetchRequest.setAttribute(QNetworkRequest::FollowRedirectsAttribute, true);
    fetchRequest.setMaximumRedirectsAllowed(5);

    QNetworkReply *reply = m_nam->get(fetchRequest);
    connect(reply, &QNetworkReply::finished, [reply, target]() {
        if (reply->error() != QNetworkReply::NoError) {
            qWarning() << Q_FUNC_INFO << "No branding file" << reply->error();
            target->setVisible(false);
        } else {
            QByteArray imgData = reply->readAll();
            QImage img = QImage::fromData(imgData);
            target->setPixmap(QPixmap::fromImage(img));
            target->setVisible(true);
        }
        reply->deleteLater();
    });
}

void RdpRunner::onStdError()
{
    QByteArray error = m_rdp->readAllStandardError();
    qDebug() << Q_FUNC_INFO << error;
}

void RdpRunner::onStdOut()
{
    QByteArray out = m_rdp->readAllStandardOutput();
    qDebug() << Q_FUNC_INFO << out;
}

void RdpRunner::onExit(int exitCode)
{
    switch (exitCode) {
    case 132:
        Q_EMIT authError();
        break;
    case 131:
        Q_EMIT connectionError();
        break;
    default:
        qDebug() << Q_FUNC_INFO << exitCode;
        break;
    };

    Q_EMIT finished(exitCode);
}

void RdpRunner::onStateChange(QProcess::ProcessState state)
{
    qDebug() << Q_FUNC_INFO << state;
    switch (state) {
    case QProcess::NotRunning:
        Q_EMIT running(false);
        break;
    case QProcess::Starting:
        Q_EMIT running(false);
        break;
    case QProcess::Running:
        Q_EMIT running(true);
        break;
    }
}

QStringList RdpRunner::args(const QString &username, const QString &domain, const QString &password) const
{
    /*
    /cert-tofu                  Automatically accept certificate on first connect
     */

    QStringList rdpArgs({
        QLatin1String("/f"), // Full screen
        QLatin1String("/from-stdin"), // blow up on errors
        QLatin1String("-toggle-fullscreen"), // Disable Alt+Ctrl+Enter
        enableFlag(QLatin1String("fonts"), m_configuration->cleartype()),
        enableFlag(QLatin1String("aero"), m_configuration->aero()),
        enableFlag(QLatin1String("window-drag"), m_configuration->windowDrag()),
        enableFlag(QLatin1String("menu-anims"), m_configuration->animations()),
        enableFlag(QLatin1String("themes"), m_configuration->themes()),
        enableFlag(QLatin1String("wallpaper"), m_configuration->wallpaper()),
        enableFlag(QLatin1String("heartbeat"), m_configuration->heartbeat()),
        enableFlag(QLatin1String("multitransport"), m_configuration->multitransport()),
        enableFlag(QLatin1String("auto-reconnect"), m_configuration->reconnect()),
        enableFlag(QLatin1String("sec-nla"), m_configuration->useCredSSP(true)),
        enableFlag(QLatin1String("compression"), m_configuration->useCompression()),
        enableFlag(QLatin1String("clipboard"), m_configuration->clipboard()),
        rdpArg(QLatin1String("u"), m_configuration->username().isEmpty() ? username : m_configuration->username()),
        rdpArg(QLatin1String("p"), m_configuration->password().isEmpty() ? password : m_configuration->password()),
        rdpArg(QLatin1String("v"), m_configuration->server()),
    });

    if (!domain.isEmpty()) {
        rdpArgs << rdpArg(QLatin1String("d"), domain);
    }

    if (m_configuration->bpp() > 0) {
        rdpArgs << rdpArg(QLatin1String("bpp"), QString::number(m_configuration->bpp()));
    }

    if (m_configuration->reconnect()) {
        rdpArgs << rdpArg(QLatin1String("auto-reconnect-max-retries"), QString::number(m_configuration->retries()));
    }

    if (m_configuration->displayControl()) {
        rdpArgs << QLatin1String("/disp");
    }

    if (m_configuration->echoChannel()) {
        rdpArgs << QLatin1String("/echo");
    }

    if (m_configuration->remoteFx()) {
        rdpArgs << QLatin1String("/rfx");
    }

    if (m_configuration->multiMon()) {
        rdpArgs << QLatin1String("/multimon");
    }

    if (m_configuration->sound()) {
        rdpArgs << QLatin1String("/sound:sys:alsa");
    }

    if (m_configuration->microphone()) {
        rdpArgs << QLatin1String("/microphone:sys:alsa");
    }

    if (m_configuration->ignoreCertificates()) {
        rdpArgs << QLatin1String("/cert-ignore");
    }

    if (m_configuration->redirectDrives()) {
        rdpArgs << QLatin1String("/drive:Drives,/media");
    }

    //qDebug() << Q_FUNC_INFO << rdpArgs.join(' ');

    return rdpArgs;
}

QString RdpRunner::enableFlag(const QString &flag, bool enable) const
{
    return (enable ? QLatin1Char('+') : QLatin1Char('-')) + flag;
}

QString RdpRunner::rdpArg(const QString &arg, const QString &value) const
{
    return QLatin1Char('/') + arg + QLatin1Char(':') + value;
}

QString RdpRunner::password() const
{
    return m_configuration->password();
}

QUrl RdpRunner::brandingUrl() const
{
    return QUrl(m_configuration->brandingUrl());
}

QString RdpRunner::termsOfService() const
{
    return m_configuration->termsOfService();
}

QString RdpRunner::domain() const
{
    return m_configuration->domain();
}

QString RdpRunner::server() const
{
    return m_configuration->server();
}

QString RdpRunner::username() const
{
    return m_configuration->username();
}
