/*
 * Copyright (C) 2017  TCOS Maintainers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "logindialog.h"
#include "rdprunner.h"
#include "ui_logindialog.h"
#include <QGuiApplication>
#include <QGroupBox>
#include <QHostInfo>
#include <QLabel>
#include <QLineEdit>
#include <QMenu>
#include <QMessageBox>
#include <QNetworkInterface>
#include <QScreen>
#include <QToolButton>
#include <QUrl>
#include <qtsystemd/logind/manager.h>

LoginDialog::LoginDialog(QWidget *parent) : QWidget(parent), ui(new Ui::LoginDialog), m_rdp(new RdpRunner(this)), m_shutdownMenu(new QMenu(this))
{
    ui->setupUi(this);
    m_shutdownMenu->addAction(QLatin1String("Power off"), this, &LoginDialog::powerOff);
    m_shutdownMenu->addAction(QLatin1String("Reboot"), this, &LoginDialog::reboot);

    ui->shutdownButton->setPopupMode(QToolButton::MenuButtonPopup);
    ui->shutdownButton->setMenu(m_shutdownMenu);

    connect(m_rdp, &RdpRunner::running, [this](bool isRunning) {
        setVisible(!isRunning);
    });
    connect(m_rdp, &RdpRunner::finished, [this](int result){
        if( runOnce() ) {
            QCoreApplication::exit(result);
        }
    });
    connect(m_rdp, &RdpRunner::authError, this, &LoginDialog::onAuthError);
    connect(m_rdp, &RdpRunner::connectionError, this, &LoginDialog::onConnectionError);

    setGeometry(QGuiApplication::primaryScreen()->availableGeometry());
}

LoginDialog::~LoginDialog()
{
    delete ui;
}

bool LoginDialog::configure(const QString &configFile)
{
    if (m_rdp->configure(configFile)) {
        ui->usernameEdit->setText(m_rdp->username());
        ui->usernameEdit->setEnabled(m_rdp->username().isEmpty());

        ui->domainEdit->setText(m_rdp->domain());
        ui->domainEdit->setEnabled(m_rdp->domain().isEmpty());

        ui->passwordEdit->setText(m_rdp->password());
        ui->passwordEdit->setEnabled(m_rdp->password().isEmpty());

        ui->serverConnection->setTitle(m_rdp->server());

        ui->stationId->setText(getStationId());

        if (m_rdp->termsOfService().isEmpty()) {
            ui->termsOfService->setVisible(false);
        } else {
            ui->termsOfService->setVisible(true);
            ui->termsOfService->setText(m_rdp->termsOfService());
        }

        if (m_rdp->brandingUrl().isEmpty()) {
            ui->branding->setVisible(false);
        } else {
            ui->branding->setVisible(true);
            ui->branding->setText(QString());
            m_rdp->populateBrandingImage(ui->branding);
        }

        return true;
    }

    return false;
}

bool LoginDialog::autoConnect() const
{
    return ( !m_rdp->username().isEmpty() && !m_rdp->password().isEmpty() );
}

void LoginDialog::doConnect() const
{
    m_rdp->connectToServer(ui->usernameEdit->text(), ui->domainEdit->text(), ui->passwordEdit->text());
    ui->passwordEdit->setText(QString());
}

QString LoginDialog::getStationId() const
{
    QString result = QLatin1String("Station ID: %1.%2(%3)");

    QString hostAddress;
    for (const QHostAddress &address : QNetworkInterface::allAddresses()) {
        if (address.protocol() == QAbstractSocket::IPv4Protocol && !address.isLoopback())
            hostAddress = address.toString();
    }

    return result.arg(QHostInfo::localHostName()).arg(QHostInfo::localDomainName()).arg(hostAddress);
}

void LoginDialog::on_shutdownButton_clicked()
{
    QCoreApplication::quit(); // FIXME: reboot/shutdown/?
}

void LoginDialog::on_connectButton_clicked()
{
    doConnect();
}

void LoginDialog::on_passwordEdit_returnPressed()
{
    doConnect();
}

void LoginDialog::onAuthError()
{
    QMessageBox::critical(this, QLatin1String("Authentication Error"),
        QLatin1String("The wrong login information was supplied.  Please try again."));
}

void LoginDialog::onConnectionError()
{
    QMessageBox::critical(this, QLatin1String("Connection Error"),
                          QLatin1String("Could not connect to host.  Please contact your systemd administrator."));
}

void LoginDialog::powerOff()
{
    Logind::Manager mgr;
    mgr.interface<org::freedesktop::login1::Manager>()->PowerOff(false);
}

void LoginDialog::reboot()
{
    Logind::Manager mgr;
    mgr.interface<org::freedesktop::login1::Manager>()->Reboot(false);
}

void LoginDialog::setRunOnce(bool runOnce)
{
    m_runOnce = runOnce;
}

bool LoginDialog::runOnce() const
{
    return m_runOnce;
}
