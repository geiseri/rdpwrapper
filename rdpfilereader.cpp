/*
 * Copyright (C) 2017  TCOS Maintainers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "rdpfilereader.h"
#include <QFile>
#include <QTextStream>

RdpFileReader::RdpFileReader() : AbstractFileReader()
{
}

bool RdpFileReader::configure(const QString &file)
{
    m_properties.clear();
    QFile rdp(file);
    if (rdp.open(QIODevice::ReadOnly)) {

        QTextStream ts(&rdp);
        ts.setCodec("UTF-8");
        ts.setAutoDetectUnicode(true);
        QString line;
        while (ts.readLineInto(&line)) {
            QStringList parts = line.trimmed().split(QLatin1Char(':'));
            if (parts.length() == 3) {
                if ( parts[1] == QLatin1String("s")) {
                    m_properties[parts[0]] = parts[2];
                } else if ( parts[1] == QLatin1String("i")) {
                    m_properties[parts[0]] = parts[2].toInt();
                }
            }
        }

        return !server(QString()).isEmpty();
    }

    return false;
}

QString RdpFileReader::server(const QString &defaultValue) const
{
    if( m_properties.contains(QLatin1String("server port")) ) {
        int port = extractMapValue<int>(m_properties, QLatin1String("full address"), 3389);
        QString address = extractMapValue<QString>(m_properties, QLatin1String("full address"), QString());
        return address + QLatin1Char(':') + QString::number(port);
    } else {
        return extractMapValue<QString>(m_properties, QLatin1String("full address"), defaultValue);
    }
}

QString RdpFileReader::username(const QString &defaultValue) const
{
    return extractMapValue<QString>(m_properties, QLatin1String("username"), defaultValue);
}

QString RdpFileReader::domain(const QString &defaultValue) const
{
    return extractMapValue<QString>(m_properties, QLatin1String("domain"), defaultValue);
}

QString RdpFileReader::password(const QString &defaultValue) const
{
    return extractMapValue<QString>(m_properties, QLatin1String("password"), defaultValue);
}

QString RdpFileReader::brandingUrl(const QString &defaultValue) const
{
    return defaultValue;
}

QString RdpFileReader::termsOfService(const QString &defaultValue) const
{
    return defaultValue;
}

int RdpFileReader::bpp(int defaultValue) const
{
    return extractMapValue<int>(m_properties, QLatin1String("session bpp"), defaultValue);
}

int RdpFileReader::retries(int defaultValue) const
{
    return extractMapValue<int>(m_properties, QLatin1String("autoreconnect max retries"), defaultValue);
}

bool RdpFileReader::remoteFx(bool defaultValue) const
{
    bool bwad = extractMapValue<int>(m_properties, QLatin1String("bandwidthautodetect"), 0) == 1;
    int contype = extractMapValue<int>(m_properties, QLatin1String("connection type"), -1);
    if (contype == -1) {
        return defaultValue;
    } else {
        if( contype == 6 ) {
            return true;
        } else {
            return contype > 5 && bwad;
        }
    }

    return false;
}

bool RdpFileReader::cleartype(bool defaultValue) const
{
    return extractMapValue<int>(m_properties, QLatin1String("allow font smoothing"), defaultValue ? 1 : 0) == 1;
}

bool RdpFileReader::aero(bool defaultValue) const
{
    return extractMapValue<int>(m_properties, QLatin1String("allow desktop composition"), defaultValue ? 1 : 0) == 1;
}

bool RdpFileReader::windowDrag(bool defaultValue) const
{
    return extractMapValue<int>(m_properties, QLatin1String("disable full window drag"), defaultValue ? 0 : 1) == 0;
}

bool RdpFileReader::animations(bool defaultValue) const
{
    return extractMapValue<int>(m_properties, QLatin1String("disable menu anims"), defaultValue ? 0 : 1) == 0;
}

bool RdpFileReader::themes(bool defaultValue) const
{
    return extractMapValue<int>(m_properties, QLatin1String("disable themes"), defaultValue ? 0 : 1) == 0;
}

bool RdpFileReader::wallpaper(bool defaultValue) const
{
    return extractMapValue<int>(m_properties, QLatin1String("disable wallpaper"), defaultValue ? 0 : 1) == 0;
}

bool RdpFileReader::heartbeat(bool defaultValue) const
{
    Q_UNUSED(defaultValue);
    return true;
}

bool RdpFileReader::multitransport(bool defaultValue) const
{
    Q_UNUSED(defaultValue);
    return true;
}

bool RdpFileReader::reconnect(bool defaultValue) const
{
    return extractMapValue<int>(m_properties, QLatin1String("autoreconnection enabled"), defaultValue ? 1 : 0) == 1;
}

bool RdpFileReader::displayControl(bool defaultValue) const
{
    Q_UNUSED(defaultValue);
    return true;
}

bool RdpFileReader::echoChannel(bool defaultValue) const
{
    Q_UNUSED(defaultValue);
    return true;
}

bool RdpFileReader::multiMon(bool defaultValue) const
{
    return extractMapValue<int>(m_properties, QLatin1String("use multimon"), defaultValue ? 1 : 0) == 1;
}

bool RdpFileReader::sound(bool defaultValue) const
{
    return extractMapValue<int>(m_properties, QLatin1String("audiomode"), defaultValue ? 0 : 2) == 0;
}

bool RdpFileReader::microphone(bool defaultValue) const
{
    return extractMapValue<int>(m_properties, QLatin1String("audiocapturemode"), defaultValue ? 1 : 0) == 1;
}

bool RdpFileReader::ignoreCertificates(bool defaultValue) const
{
    return extractMapValue<int>(m_properties, QLatin1String("authentication level"), defaultValue ? 0 : 1) == 0;
}

bool RdpFileReader::redirectDrives(bool defaultValue) const
{
    return extractMapValue<QString>(m_properties, QLatin1String("drivestoredirect"),
               defaultValue ? QLatin1String("DynamicDrives") : QString())
            == QLatin1String("DynamicDrives");
}

bool RdpFileReader::useCredSSP(bool defaultValue) const
{
    return extractMapValue<int>(m_properties, QLatin1String("enablecredsspsupport"), defaultValue ? 1 : 0) == 1;
}

bool RdpFileReader::useCompression(bool defaultValue) const
{
    return extractMapValue<int>(m_properties, QLatin1String("compression"), defaultValue ? 1 : 0) == 1;
}

bool RdpFileReader::clipboard(bool defaultValue) const
{
    return extractMapValue<int>(m_properties, QLatin1String("clipboard"), defaultValue ? 1 : 0) == 1;
}
