/*
 * Copyright (C) 2017  TCOS Maintainers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LOGINDIALOG_H
#define LOGINDIALOG_H

#include <QWidget>

namespace Ui
{
class LoginDialog;
}
class RdpRunner;
class QMenu;

class LoginDialog : public QWidget
{
    Q_OBJECT

public:
    explicit LoginDialog(QWidget *parent = 0);
    ~LoginDialog();

    bool configure(const QString &configFile);
    bool autoConnect() const;
    void doConnect() const;
    bool runOnce() const;
    void setRunOnce(bool runOnce);

private:
    QString getStationId() const;

private slots:
    void on_shutdownButton_clicked();
    void on_connectButton_clicked();
    void on_passwordEdit_returnPressed();

    void onAuthError();
    void onConnectionError();

    void powerOff();
    void reboot();

private:
    Ui::LoginDialog *ui;
    RdpRunner *m_rdp;
    bool m_runOnce;
    QMenu *m_shutdownMenu;
};

#endif // LOGINDIALOG_H
